# Online Generalized Eigenvalue Decomposition

The online version of Generalized Eigenvalue Decomposition(GEVD)



- OGEVD.m:

Online Generalized EigneValue Decomposition (OGEVD)

```
[W, CN, CDN] = OGEVD(W, CN, CDN, Comp)
OGEVD with specified numerator and denumerator

[W, CN, CDN] = OGEVD(W, CN, CDN, Comp, betta, n1, n2)
OGEVD with updating numerator and specified denumerator

[W, CN, CDN] = OGEVD(W, CN, CDN, Comp, betta, n1, n2, flg)
OGEVD with updating fourth order numerator and specified denumerator

[W, CN, CDN] = OGEVD(W, CN, CDN, Comp, betta, n1, n2, gamma, dn1, dn2)
OGEVD with updating second order numerator and denumerator

[W, CN, CDN] = OGEVD(W, CN, CDN, Comp, betta, n1, n2, gamma, dn1, dn2, flg)
OGEVD with updating fourth order numerator and second order denumerator

input:
  W: demixing matrix
  CN: numerator posetive definite matrix
  CDN: denumerator posetive definite matrix
  invCDN: inversion of the CDN matrix
  Comp: number of pricipal component
  betta: forgetting factor for updating numerator matrix
  n1: the first element in updating the numerator matrix
  n2: the current element in updating the numerator matrix
  gamma: forgetting factor for updating denumerator matrix
  dn1: the first element in updating the denumerator matrix
  dn2: the second element in updating the denumerator matrix
  flg: using the fourth order
  CN4: the last 4th order matrix
  
output:
  W: demixing matrix updated concering the current data
  CN: numerator matrix updated concering the current data
  CDN: denumerator matrix updated concering the current data
  CN4: 4th order matrix updated concering the current data
  invCDN: inversion of the CDN matrix updated concering the current data
```

- testOGEVD.m:

A test code for Online Generalized Eigenvalue Decomposition


- SampleFECGDaISy

Sample data for testOGEVD.m
 
