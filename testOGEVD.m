% A test code for Online Generalized Eigenvalue Decomposition
% 
%
% Fahimeh Jamshidian Tehrani,
% jamshidian.t@cse.shirazu.ac.ir,
% fahimeh.jt@gmail.com
% February 2020
%
%


clc
% clear all
close all

%----DaISY----
load SampleFECGDaISy.mat;
x = data{1};
% Number of thoracic channel
ThoracicChannel = 3;
% Selected maternal channel for peak detection
smch = 8;
[CH, N] = size(x);

plotflag = 1;
% Average mECG peak detection rate
average_peak_detection_rate = 1.1;


% OGEVD parameter initializing
betta = 1;
gamma = 1;
alpha1 = .3;
alpha2 = .7;
alpha3 = .5;
Comp = CH;
CN_taum = alpha2 * eye(Comp);
CN_4 = alpha2 * eye(Comp);
CDN_x = alpha1 * eye(Comp);
invCDN_x = alpha1 * eye(Comp);
W_taum_x = alpha3 * eye(Comp);
flg4Order = 0;

% finding peaks of the referenced mathernal signal
peaks = PeakDetection(x(smch,:), average_peak_detection_rate/fs{1}, 1);
Peakloc = find(peaks);

T = round(mean(diff(Peakloc)));
[T0,T1] = SynchPhaseTimes2(peaks);
be4peak = 1: T0(1)-1;
T0 = [be4peak T0];
T1 = [be4peak+round(T) T1];
OGEVDComp = zeros(CH, N);
N = length(T0);

% OGEVD Decomposition
for ti = 1 : N
    % OGEVD
    [W_taum_x, CN_taum, CDN_x, CN_4, invCDN_x] = OGEVD(W_taum_x, CN_taum, CDN_x, invCDN_x, Comp, betta, x(:, T1(ti)), x(:, T0(ti)), gamma, x(:, ti), x(:, ti), flg4Order, CN_4);
    
    OGEVDComp(:, ti) = W_taum_x' * x(:, ti);
end

% Ploting
if(plotflag == 1)
    figure,
    plot(x(smch,:));
    hold on
    plot(Peakloc, x(smch,Peakloc),'ro');
    title('Maternal R-peak locations');
    
    PlotECG(x, CH, 'b', 1, 'Original Data');
    
    PlotECG(OGEVDComp,CH,'b',1,'OGEVD Component'); 
end





