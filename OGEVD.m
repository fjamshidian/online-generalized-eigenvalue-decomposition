
function [W, CN, CDN, CN4, invCDN] = OGEVD(W, CN, CDN, invCDN, Comp, varargin)
%
% [W, CN, CDN, CN4, invCDN] = OGEVDinv(W, CN, CDN, invCDN, Comp, betta, n1, n2, gamma, dn1, dn2, flg, CN4)
% Online Generalized EigneValue Decomposition (OGEVD)
% proposed in Fatemi M, Sameni R. "An online subspace denoising algorithm for maternal ecg removal from fetal ecg signals." 
% Iranian Journal of Science and Technology, Transactions of Electrical Engineering. 2017 Mar 1;41(1):65-79.
%
% Fahimeh Jamshidian Tehrani,
% jamshidian.t@cse.shirazu.ac.ir,
% fahimeh.jt@gmail.com
% February 2020
% [W, CN, CDN] = OGEVD(W, CN, CDN, Comp)
% OGEVD with specified numerator and denumerator
%
% [W, CN, CDN] = OGEVD(W, CN, CDN, Comp, betta, n1, n2)
% OGEVD with updating numerator and specified denumerator
%
% [W, CN, CDN] = OGEVD(W, CN, CDN, Comp, betta, n1, n2, flg)
% OGEVD with updating fourth order numerator and specified denumerator
%
% [W, CN, CDN] = OGEVD(W, CN, CDN, Comp, betta, n1, n2, gamma, dn1, dn2)
% OGEVD with updating second order numerator and denumerator
%
% [W, CN, CDN] = OGEVD(W, CN, CDN, Comp, betta, n1, n2, gamma, dn1, dn2, flg)
% OGEVD with updating fourth order numerator and second order denumerator
%
% input:
%   W: demixing matrix
%   CN: numerator posetive definite matrix
%   CDN: denumerator posetive definite matrix
%   invCDN: inversion of the CDN matrix
%   Comp: number of pricipal component
%   betta: forgetting factor for updating numerator matrix
%   n1: the first element in updating the numerator matrix
%   n2: the current element in updating the numerator matrix
%   gamma: forgetting factor for updating denumerator matrix
%   dn1: the first element in updating the denumerator matrix
%   dn2: the second element in updating the denumerator matrix
%   flg: using the fourth order
%   CN4: the last 4th order matrix
% output:
%   W: demixing matrix updated concering the current data
%   CN: numerator matrix updated concering the current data
%   CDN: denumerator matrix updated concering the current data
%   CN4: 4th order matrix updated concering the current data
%   invCDN: inversion of the CDN matrix updated concering the current data
%


% parameter initialization
if nargin == 13 || nargin == 9
    flg = varargin{end-1};
else
    flg = 0;
end

if nargin > 6
    betta = varargin{1};
    n1 = varargin{2};
    n2 = varargin{3};
    CN = betta * CN + n1 * n2';
end
if nargin > 10
    gamma = varargin{4};
    dn1 = varargin{5};
    dn2 = varargin{6};
    CDN = gamma * CDN + dn1 * dn2';
end

CN = (CN + CN')/2;
CDN = (CDN + CDN')/2;
CN4 = CN;
Chat = CN;

if flg > 0   % order 4
    CN4 = varargin{end};
    CN4 = betta*CN4 + n2' * pinv(CN) * n1*n1 * n2';
    CN4 = (CN4 + CN4')/2;
    Chat = CN4;
end

% invCDN = pinv(CDN);
% Sequential inversion
invCDN = gamma * invCDN - (gamma * invCDN * dn1 * dn2' * invCDN)/(gamma^(-1) + dn2' * invCDN * dn1);
invCDN = (invCDN+invCDN')/2;

% Online generalized eigenvalue decomposition
for j = 1 : Comp
    wj = (W(:,j)' * CDN * W(:,j)) / (W(:,j)' * Chat * W(:,j)) * (invCDN * Chat * W(:,j));
    wj = wj / norm(wj);
    Chat = Chat - ((Chat * (wj * wj'))/(wj' * Chat * wj))*Chat;
    W(:, j) = wj;
end


% d = W'*CDN*W;
% [YY,I] = sort(diag(d));
% I = I(end:-1:1);

% 
% W = W(:,I);